from django.db import models
from django.contrib.auth import get_user_model
from django.utils import timezone
# Create your models here.

class Article(models.Model):
    title = models.CharField(max_length=256)
    slug = models.SlugField(unique=True,)
    author = models.ForeignKey(get_user_model(), verbose_name="author", on_delete=models.CASCADE)
    content = models.TextField()
    publish = models.DateTimeField(default=timezone.now, verbose_name='publish time')
    created = models.DateTimeField(auto_now_add=True, verbose_name='create time')
    updated = models.DateTimeField(verbose_name='updated in', auto_now=True)
    status = models.BooleanField(default=False)

    def __str__(self):
        return self.title