from django.shortcuts import render
from .models import Article
from django.views.generic import ListView
# Create your views here.

class ArticleList(ListView):
    template_name = 'articlelist.html'
    model = Article
    context_object_name = 'data'