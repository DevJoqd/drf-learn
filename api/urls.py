from django.urls import path, include
from . import views as api_views

urlpatterns = [
    path('article-list/', api_views.ArticleListApi.as_view(), name='articles_api'),
    path('article-list-create/', api_views.ArticleListCreateApi.as_view(), name='articles_api'),
    path('article-create/', api_views.ArticleCreateApi.as_view(), name='article_create'),
    path('article/<slug:test>/', include(
            [
                path('', api_views.ArticleRetreive.as_view(), name='article_api'),
                path('remove/', api_views.ArticleRetreiveDestory.as_view(), name='article_destroy_api'),
                path('update/', api_views.ArticleRetreiveUpdate.as_view(), name='article_update_api'),
                path('full/', api_views.FullAccessArticleApi.as_view(), name='article_full_api'),
            ]
        )
    ),
]
