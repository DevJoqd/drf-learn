from rest_framework import serializers
from blog.models import Article

# Serializer is a class that convert python data type like dictionary to json format
class ArticleSerializer(serializers.ModelSerializer):
    # Model serializer can serialize a model with specific fields
    class Meta:
        model = Article
        # fields = ['title', 'slug', 'author', 'content']
        # exclude = ['updated', 'created', 'publish', 'status']
        # exclude meaning get all field except these fields
        fields = '__all__'